#!/bin/bash

for i in *.gif ; do
	ffmpeg -i "$i" -c:v libvpx -crf 4 -b:v 500K "$(basename "${i/.gif}").webm" && rm $i
done
